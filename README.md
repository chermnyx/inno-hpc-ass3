# HPC PA-II (p02)

Dmitrii Chermnykh, <d.chermnykh@innopolis.university>

## Task 1

![GIF_task1](./t1.gif)

## Task 2

![GIF_task2](./t2.gif)

## Task 3

![PNG_task3](./t3.png)

As you can see, *linear congruential generator* provides better result as it distributes points more uniformly compared to standard `rand()`.

## Task 4

Let's use the following integral from MA-1 course: 

```math
\int_{0.75}^2{dx \over \sqrt{2 + 3x - 2x^2}} = {\pi \over 2 \sqrt{2}} \;\;\;\text{(from answers)}
```

## Task 5

![PNG_task5](./t5.png)

*Linear congruential generator* again produces better result.
