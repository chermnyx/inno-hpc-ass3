#include <bits/stdc++.h>
#include <ext/stdio_filebuf.h>

#define SEED 1234098
#define STEPS 100

#define GNUPLOT_COMMAND "gnuplot -persist"

std::ostream *get_gnuplot() {
    FILE *gnuplotfd = popen(GNUPLOT_COMMAND, "w");
    if (gnuplotfd == nullptr)
        throw std::runtime_error("UNABLE TO START GNUPLOT");
    auto filebuf = new __gnu_cxx::stdio_filebuf<char>(gnuplotfd, std::ios::out);
    auto plot = new std::ostream(filebuf);
    return plot;
}

double randrange(double a, double b, std::function<double()> rand_fn, double rnd_max) {
    return a + double(rand_fn()) / rnd_max * (b - a);
}

struct point {
    double x;
    double y;
};

double nlog(double base, double x) {
    return std::log(x) / std::log(base);
}

double calcPi(size_t npts, std::function<double()> rand_fn, double rnd_max,
              std::vector<point> &pts_inside,
              std::vector<point> &pts_outside) {
    double inside = 0;
    double outside = 0;

    for (auto i = 0; i < npts; ++i) {
        auto x = randrange(-1, 1, rand_fn, rnd_max);
        auto y = randrange(-1, 1, rand_fn, rnd_max);

        if (x * x + y * y < 1) {
            pts_inside.push_back(point{x, y});
            inside += 1;
        } else {
            pts_outside.push_back(point{x, y});
            outside += 1;
        }

    }

    double pi = 4 * inside / npts;
    return pi;
}

static std::vector<unsigned long long> NNN;
static std::vector<double> acc_1;
static std::vector<double> acc_2;

void task1() {
    auto gif = get_gnuplot();

    *gif << R"EOF(set term gif animate optimize delay 1 size 1024
set output "t1.gif"

set xlabel "x"
set ylabel "y"
set size square

set xrange[-1:1]
set yrange[-1:1]
set datafile separator whitespace
)EOF";

    std::cout << "### Task 1" << std::endl;
    auto step = std::pow(30000.0 / 1000.0, 1.0 / (STEPS - 1));

    for (auto NN = 1000.0; NN <= 30000.0; NN *= step) {
        unsigned long long N = std::floor(NN);
        NNN.push_back(N);
        std::cout << "N=" << N << std::endl;

        std::vector<point> inside;
        std::vector<point> outside;
        auto v = calcPi(N, rand, RAND_MAX, inside, outside);
        acc_1.push_back(v - std::numbers::pi);

        *gif << "set title \"N=" << N << ", π≈" << v << "\"\n";
        *gif << "plot '-' using 1:2 with points linecolor rgb \"green\" notitle,\\\n"
                "'-' using 1:2 with points linecolor rgb \"red\"   notitle\n";

        for (auto pt: inside) {
            *gif << pt.x << " " << pt.y << "\n";
        }
        *gif << "e" << std::endl;

        for (auto pt: outside) {
            *gif << pt.x << " " << pt.y << "\n";
        }
        *gif << "e" << std::endl;

    }
    *gif << "exit" << std::endl;
}

class LCG {
public:
    const unsigned long long a = 845;
    unsigned long long X;
    const unsigned long long c = 2625;
    const unsigned long long m = 8192;

    explicit LCG(unsigned long long seed) : X(seed) {};

    unsigned long long rand() {
        auto ret = X;
        X = (a * X + c) % m;
        return ret;
    }
};

void task2() {
    auto gif = get_gnuplot();

    auto rng = LCG(SEED);

    *gif << R"EOF(set term gif animate optimize delay 1 size 1024
set output "t2.gif"

set xlabel "x"
set ylabel "y"
set size square

set xrange[-1:1]
set yrange[-1:1]
set datafile separator whitespace
)EOF";

    std::cout << "### Task 2" << std::endl;
    auto step = std::pow(30000.0 / 1000.0, 1.0 / (STEPS - 1));

    for (auto NN = 1000.0; NN <= 30000.0; NN *= step) {
        unsigned long long N = std::floor(NN);
        std::cout << "N=" << N << std::endl;

        std::vector<point> inside;
        std::vector<point> outside;
        auto v = calcPi(N, [&rng]() { return rng.rand(); },
                        rng.m, inside, outside);
        acc_2.push_back(v - std::numbers::pi);

        *gif << "set title \"N=" << N << ", π≈" << v << "\"\n";
        *gif << "plot '-' using 1:2 with points linecolor rgb \"green\" notitle,\\\n"
                "'-' using 1:2 with points linecolor rgb \"red\"   notitle\n";

        for (auto pt: inside) {
            *gif << pt.x << " " << pt.y << "\n";
        }
        *gif << "e" << std::endl;

        for (auto pt: outside) {
            *gif << pt.x << " " << pt.y << "\n";
        }
        *gif << "e" << std::endl;

    }
    *gif << "exit" << std::endl;
}

void task3() {
    auto g = get_gnuplot();

    auto step = std::pow(30000.0 / 1000.0, 1.0 / (STEPS - 1));


    *g << R"EOF(
set term png size 1024
set output "t3.png"
set xlabel "N"
set ylabel "ERR"
set logscale x )EOF" << step << R"EOF(
set title "Task 3"
plot '-' using 1:2 w lines title "rand()" linecolor rgb "red",\
     '-' using 1:2 w lines title "LCG" linecolor rgb "blue"
)EOF";

    for (auto i = 0; i < NNN.size(); ++i) {
        *g << NNN[i] << " " << acc_1[i] << "\n";
    }
    *g << "e" << std::endl;

    for (auto i = 0; i < NNN.size(); ++i) {
        *g << NNN[i] << " " << acc_2[i] << "\n";
    }
    *g << "e" << std::endl;
    *g << "exit" << std::endl;
}

// a function from MA-I assignment
double f(double x) {
    return 1 / std::sqrt(2 + 3 * x - 2 * x * x);
}

static double a = 0.75;
static double b = 2;
static double int_result = std::numbers::pi / 2 / std::sqrt(2);

double integrate(size_t N,
                 std::function<double()> rand_fn, double rnd_max) {
    double s = 0;
    for (auto i = 0; i < N; ++i) {
        auto r = rand_fn() / rnd_max;
        auto x = a + r * (b - a);
        auto y = f(x);
        s += y;
    }
    return (b - a) * s / N;
}

void task4_5() {
    auto g = get_gnuplot();

    auto step = std::pow(1000000.0 / 100.0, 1.0 / (STEPS - 1));

    *g << R"EOF(
set term png size 1024
set output "t5.png"
set xlabel "N"
set ylabel "ERR"
set logscale x )EOF" << step << R"EOF(
set title "Task 5"
plot '-' using 1:2 w lines title "rand()" linecolor rgb "red",\
     '-' using 1:2 w lines title "LCG" linecolor rgb "blue"
)EOF";

    auto rng = LCG(SEED);
    std::vector<unsigned long long> NNNN;
    std::vector<double> vv1;
    std::vector<double> vv2;

    std::cout << "### Task 5" << std::endl;

    for (auto NN = 100.0; NN <= 1000000.0; NN *= step) {
        unsigned long long N = std::floor(NN);
        std::cout << "N=" << N << std::endl;
        auto v1 = integrate(N, rand, RAND_MAX);
        auto v2 = integrate(N,
                            [&rng]() { return rng.rand(); },
                            rng.m);
        NNNN.push_back(N);
        vv1.push_back(v1 - int_result);
        vv2.push_back(v2 - int_result);
    }

    for (auto i = 0; i < NNNN.size(); ++i) {
        *g << NNNN[i] << " " << vv1[i] << "\n";
    }
    *g << "e" << std::endl;

    for (auto i = 0; i < NNNN.size(); ++i) {
        *g << NNNN[i] << " " << vv2[i] << "\n";
    }
    *g << "e" << std::endl;
    *g << "exit" << std::endl;
}

int main() {
    srand(SEED);
    task1();
    task2();
    task3();
    srand(SEED);
    task4_5();
    return 0;
}
